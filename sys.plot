MAXMEM=4000 # MB
set term png medium size 4500,750
set output "sys.png"
set xlabel "time"
set ylabel "percent CPU usage"
set timefmt "%s"
set xdata time
set format x "%H:%M:%S"
set xtics out
set y2label "memory (MB)"
set y2tics
set yrange [0:400]
set y2range [0:MAXMEM]
set y2tics nomirror
set ytics nomirror
set grid noxtics ytics layerdefault front

# load external file created by siril output and siril_log_to_dat.awk
#load 'labels.plot'

plot \
	"sys.dat" using 1:($2+$3+$4+$5) with filledcurves above x1 axis x1y1 lt rgb "#e5ffdf" title "CPU 4", \
	"sys.dat" using 1:($2+$3+$4) with filledcurves above x1 axis x1y1 lt rgb "#ffffdf" title "CPU 3", \
	"sys.dat" using 1:($2+$3) with filledcurves above x1 axis x1y1 lt rgb "#ffefdf" title "CPU 2", \
	"sys.dat" using 1:2 with filledcurves above x1 axis x1y1 lt rgb "#ffdfdf" title "CPU 1", \
	"sys.dat" using 1:6 with lines axis x1y1 lt rgb "#00ff00" title "User", \
	"sys.dat" using 1:7 with lines axis x1y1 lt rgb "#ffb700" title "System", \
	"sys.dat" using 1:8 with lines axis x1y1 lt rgb "#ff0000" title "wait I/O", \
	"sys.dat" using 1:9 with lines axis x1y2 lt rgb "#2525ff" title "avail mem"
