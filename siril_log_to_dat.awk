BEGIN { FS="[: ]" }
/running command/ && !/cd$/ && !/set/ {
	print "set arrow from " $1 ", graph 0 to " $1 ", graph 1 nohead front" ;
	print "set label at " $1+5 ",320 '" $5 "' rotate by 90 front"
}

